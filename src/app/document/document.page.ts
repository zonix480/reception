import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { GlobalProvider} from "../../providers/global";
import { UserProvider} from "../../providers/users/user";
import { Router } from '@angular/router';
@Component({
  selector: 'app-document',
  templateUrl: './document.page.html',
  styleUrls: ['./document.page.scss'],
})
export class DocumentPage implements OnInit {
  public documentForm: FormGroup;
  isother:boolean;
  id:number;
  type:number;
  newId:number;
  constructor(public formBuilder: FormBuilder,private usersProv: UserProvider, 
    private globalProv: GlobalProvider,public router:Router) {
    this.documentForm = formBuilder.group({
      document: ['', Validators.compose([Validators.pattern('[0-9\/]*'), Validators.required])],
      other: ['', Validators.compose([Validators.maxLength(60), Validators.pattern('[a-zA-Z ]*')])],
      type: ['', Validators.compose([Validators.required])],
    });
  }

  ngOnInit() {
  }

  other(){
    this.isother = true;
  }

  another(){
    this.isother = false;
  }


  /**
  * Method
  */
 verify() {
  //call provider API
  if (!this.documentForm.valid) {
    this.globalProv.presentToast("Revisa los datos ingresados");
  }else{
    let requestnew = {
      type : this.documentForm.controls.other.value
    }
    if(this.documentForm.controls.type.value == "other"){
      this.globalProv.showLoader();
      this.usersProv.setCallPromise('post', requestnew, 'othertype', null).then(res => {
      this.globalProv.hideLoader();      
        if(!res.success) {
          this.globalProv.presentToast(res.message);
        }else if(res.success) {
          this.newId = res.data;
          this.globalProv.setStorage('document', {
            document: this.documentForm.controls.document.value,
            type: res.data
          });
          console.log("Bien creada la nueva "+ res.data);
        }
      });
    }else{
      this.id =this.documentForm.controls.document.value;
      this.type =this.documentForm.controls.type.value;
    }
  this.globalProv.showLoader();
    this.usersProv.setCallObservable('get', 'userby/'+this.id+"/"+this.type, null).subscribe
    (data => {
      this.globalProv.hideLoader();
      if (!data.success) {
        this.globalProv.presentToast(data.message);
        if(this.documentForm.controls.type.value != "other"){
          this.globalProv.setStorage('document', {
            document: this.id,
            type: this.type
          });
        } 
        this.router.navigateByUrl("/registerform");
      } else if (data.success) {
        this.globalProv.setStorage('document', {
          document: this.id,
          type: this.type
        });
        this.globalProv.presentToast("Son estos tus datos?");
        this.router.navigateByUrl("/data");
      }
    }); 
  }
}




}
