import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';


import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import {HttpClientModule} from "@angular/common/http";

import { UserProvider } from '../providers/users/user';
import { EpsProvider } from '../providers/eps/eps';
import { ArlProvider } from '../providers/arl/arl';
import { CompanyProvider } from '../providers/companies/company';
import { GlobalProvider } from '../providers/global';
import { ApiProvider } from '../providers/api';
import { RhProvider } from '../providers/rh/rh';
import { RulesProvider } from '../providers/rules/rules';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';


import { FormBuilder, ReactiveFormsModule} from '@angular/forms';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';




@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), HttpClientModule, AppRoutingModule, ReactiveFormsModule],
  providers: [
    StatusBar,
    Camera,
    SplashScreen,
    NativeStorage,
    ApiProvider,
    GlobalProvider,
    UserProvider,
    ArlProvider,
    EpsProvider,
    RulesProvider,
    RhProvider,
    CompanyProvider,
    FormBuilder,
    ReactiveFormsModule, 
    BarcodeScanner, 
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
