import { Component } from '@angular/core';
import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner/ngx';
import { GlobalProvider} from "../../providers/global";
import { UserProvider} from "../../providers/users/user";
import { Router } from '@angular/router';
import { Platform } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { AlertController } from '@ionic/angular';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  public configuration = {
    id:"",
    color_principal: "",
    color_secondary: "",
    terms: "",
    logo:""
  }

  data={};
  test={
    lastname:{},
    name:{},
    rh:{},
  };
  base64Image="";
  public phone:boolean;
  public ce;
  public ipad:boolean;
  option:BarcodeScannerOptions;

  constructor(private globalProv: GlobalProvider,
    public alertController: AlertController,
    private barcodeScanner: BarcodeScanner,
    private usersProv: UserProvider,
    private camera:Camera,

    public router: Router,public plt: Platform) {

    }
    ngOnInit() {
      this.globalProv.getStorage("configuration").then(res => {
        this.configuration.id = res.id,
        this.configuration.color_principal = res.color_principal,
        this.configuration.color_secondary = res.color_secondary,
        this.configuration.logo = res.logo
      })
      console.log("HOLA");
      console.log(this.configuration);
    }

  scan () {
    this.option = {
      prompt:"Escanea el codigo QR",
      resultDisplayDuration :0,
      formats:"QR_CODE,PDF_417,UPC_A,UPC_E,DATA_MATRIX,CODABAR,AZTEC",
      orientation : "landscape",
      showTorchButton : true, // iOS and Android
    }
    this.barcodeScanner.scan(this.option).then(barcodeData => {
      this.test.lastname = barcodeData.text.substring(58,98).replace(/ /g, "");
      //this.test.name = barcodeData.text.substring(98,109);
      this.test.name = barcodeData.text.substring(98,138).replace(/ /g, "");
      this.test.rh = barcodeData.text.substring(166,168);
      this.data = barcodeData.text.substring(48,58);
      if(this.data[0].substring(0,1)== '0'){
        this.data = barcodeData.text.substring(50, 58);
        this.globalProv.presentToast(this.data[0].substring(2,9));
      }
      this.globalProv.setStorage('cedula', {
        name: this.test.name,
        lastname: this.test.lastname,
        rh: this.test.rh
      });
    //Escanear y continuar en app  
      this.presentAlertConfirm(this.data);
     }).catch(err => {
         console.log('Error', err);
     });
  }

  openbrowser(){
    window.open('https:www.google.com', '_system');
  }


  async presentAlertConfirm(cedula) {
    const alert = await this.alertController.create({
      header: '¿ Es correcto ?',
      message: 'Es este el numero de tu cedula: <strong>'+ cedula+'</strong><br>',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Confirmar',
          handler: () => {
            this.verify(cedula);
          }
        }
      ]
    });
    await alert.present();
  }
  

  /**
  * Method
  */
 verify(cedula) {
  //call provider API
  let id = cedula;
  let type = 1;
  this.globalProv.showLoader();
    this.usersProv.setCallObservable('get', 'userby/'+id+"/"+type, null).subscribe
    (data => {
      this.globalProv.hideLoader();
      if (!data.success) {
        this.globalProv.presentToast(data.message);
        this.globalProv.setStorage('document', {
          document: id,
          type: type
        });
        this.router.navigateByUrl("/registerform");
      } else if (data.success) {
        this.globalProv.setStorage('document', {
          document: id,
          type: type
        });
        this.globalProv.presentToast("Son estos tus datos?");
        this.router.navigateByUrl("/data");
      }
    }); 
}



}
