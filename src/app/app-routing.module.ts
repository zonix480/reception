import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'codeqr', loadChildren: './codeqr/codeqr.module#CodeqrPageModule' },
  { path: 'registerform', loadChildren: './registerform/registerform.module#RegisterformPageModule' },
  { path: 'completeregister', loadChildren: './completeregister/completeregister.module#CompleteregisterPageModule' },
  { path: 'rules', loadChildren: './rules/rules.module#RulesPageModule' },
  { path: 'document', loadChildren: './document/document.module#DocumentPageModule' },
  { path: 'data', loadChildren: './data/data.module#DataPageModule' },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
