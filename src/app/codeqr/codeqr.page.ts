import { Component, OnInit } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { GlobalProvider} from "../../providers/global";
import { Router } from '@angular/router';
@Component({
  selector: 'app-codeqr',
  templateUrl: './codeqr.page.html',
  styleUrls: ['./codeqr.page.scss'],
})
export class CodeqrPage implements OnInit {

  constructor(private globalProv: GlobalProvider,
    private barcodeScanner: BarcodeScanner,
    public router: Router) { }

  ngOnInit() {
    this.barcodeScanner.scan().then(barcodeData => {
      this.globalProv.presentToast('Leido '+barcodeData);
      console.log('Barcode data', barcodeData);
     }).catch(err => {
         console.log('Error', err);
     });
  }

  
}
