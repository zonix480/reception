import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CodeqrPage } from './codeqr.page';

describe('CodeqrPage', () => {
  let component: CodeqrPage;
  let fixture: ComponentFixture<CodeqrPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CodeqrPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CodeqrPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
