import { Component, OnInit } from '@angular/core';
import { GlobalProvider} from "../../providers/global";
import { UserProvider} from "../../providers/users/user";
import { EpsProvider} from "../../providers/eps/eps";
import { ArlProvider} from "../../providers/arl/arl";
import { RhProvider} from "../../providers/rh/rh";
import { CompanyProvider} from "../../providers/companies/company";
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
//Para evaluar formularios el formbuilder
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router } from '@angular/router';



@Component({
  selector: 'app-registerform',
  templateUrl: './registerform.page.html',
  styleUrls: ['./registerform.page.scss'],
})
export class RegisterformPage implements OnInit {

  public registerForm: FormGroup;
  public eps = [];
  public arl = [];
  public rh = [];
  public data={};
  public rhexistname = "";
  public base64Image;
  public idavatar;
  public epsselected;
  public document;
  public rhexist;
  public type;
  public companies = [];
  public arlselected;
  public companyselectnew;
  public contact;
  public name;
  public lastname;
  public nameexist;
  public lastnameexist;
  public rhcedula;
  public telcontact;
  public rhselect;
  public companyselected;
  constructor(private usersProv: UserProvider, 
              private globalProv: GlobalProvider,
              private RhProvider: RhProvider,
              private EpsProv: EpsProvider,
              private companyProv: CompanyProvider,
              private ArlProv: ArlProvider,
              public formBuilder: FormBuilder,
              public router: Router,
              private camera:Camera) {
                this.registerForm = formBuilder.group({
                  name: ['', Validators.compose([Validators.maxLength(60), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
                  lastname: ['', Validators.compose([Validators.maxLength(60), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
                  phone: ['', Validators.compose([Validators.minLength(10), Validators.maxLength(10), Validators.pattern('[0-9\/]*'), Validators.required])],
                  eps: ['', Validators.compose([Validators.required])],
                  arl: ['', Validators.compose([Validators.required])],
                  othereps: ['', Validators.compose([Validators.maxLength(60), Validators.pattern('[a-zA-Z ]*')])],
                  otherarl: ['', Validators.compose([Validators.maxLength(60), Validators.pattern('[a-zA-Z ]*')])],
                  othercompany: ['', Validators.compose([Validators.maxLength(60), Validators.pattern('[a-zA-Z ]*')])],
                  company: ['', Validators.compose([Validators.required])],
                  email: ['', Validators.compose([Validators.maxLength(120), Validators.email, Validators.required])],
                  rh: ['', Validators.compose([Validators.required])],
                  contact: ['', Validators.compose([])],
                  telcontact: ['', Validators.compose([Validators.minLength(10), Validators.maxLength(13), Validators.pattern('[0-9\/]*')])],
              });
        

  }

  ngOnInit() {
    this.globalProv.getStorage("document").then(res => {
      this.document = res.document;
      this.type = res.type;
    })
    this.globalProv.getStorage("cedula").then(res => {
      this.nameexist = res.name;
      this.lastnameexist = res.lastname;
      this.rhcedula = res.rh;
      this.getRhSelect(this.rhcedula);
    })
    this.getEps();
    this.getArl();
    this.getCompanies();
    this.getRh();
  }

  public onChangeTimeRh(rhsele){
    this.rhselect = rhsele;
    console.log("Cambio a esto "+this.rhselect);
  }

  public onChangeTimeCompany(company){
    this.companyselected = company;
    this.companyselectnew = company;
    console.log("Cambio a esto Compania "+this.companyselectnew);
  }


  register(){
      if (!this.registerForm.controls.name.valid) {
        this.globalProv.presentToast("Revisa los NOMBRES ingresados");
      }
      if (!this.registerForm.controls.lastname.valid) {
        this.globalProv.presentToast("Revisa los APELLIDOS ingresados");
      }
      if (!this.registerForm.controls.phone.valid) {
        this.globalProv.presentToast("Revisa el TELEFONO ingresados");
      }
      if (!this.registerForm.controls.company.valid) {
        this.globalProv.presentToast("Revisa la EMPRESA ingresados");
      }
      if (!this.registerForm.controls.email.valid) {
        this.globalProv.presentToast("Revisa el EMAIL ingresados");
      }
      if (!this.registerForm.controls.rh.valid) {
        this.globalProv.presentToast("Revisa el RH ingresados");
      }
      if (!this.registerForm.controls.eps.valid) {
        this.globalProv.presentToast("Revisa la EPS ingresados");
      }
      if (!this.registerForm.controls.arl.valid) {
        this.globalProv.presentToast("Revisa la ARL datos ingresados");
      }
      if (!this.registerForm.controls.contact.valid) {
        this.globalProv.presentToast("Revisa la PERSONA ingresados");
      }
      if (!this.registerForm.controls.telcontact.valid) {
        this.globalProv.presentToast("Revisa la NUMPERSONA datos ingresados");
      }else{
        if(this.epsselected == "other"){
          this.eps
        }
        if(this.registerForm.controls.contact.value == ""){
          this.contact = "No tiene";
        }else{
          this.contact = this.registerForm.controls.contact.value;
        }
        if(this.registerForm.controls.telcontact.value == ""){
          this.telcontact = "No tiene";
        }else{
          this.telcontact = this.registerForm.controls.telcontact.value;
        }
        this.globalProv.showLoader();
        if(this.rhexistname != ""){
          this.rhselect = this.rhexist;
        }
        if(this.registerForm.value.company == "other"){
          this.companyselected = 1;
          this.companyselectnew = "other";
        }
        console.log("ESTE ES EL RH QUE SELECCIONOOOO "+this.rhselect);
        let request = {
                document: this.document,
                type: this.type,
                name: this.registerForm.value.name,
                lastname: this.registerForm.value.lastname,
                phone: this.registerForm.value.phone,
                eps: this.epsselected,
                arl: this.arlselected,
                email: this.registerForm.value.email,
                company: this.companyselected,
                contact: this.contact,
                rh: this.rhselect,
                numcontact: this.telcontact
            };

        //call provider API
        this.usersProv.setCallPromise('post', request, 'registerperson', null).then(res => {
          this.globalProv.hideLoader();      
            if(!res.success) {
              this.globalProv.presentToast(res.message);
            }else if(res.success) {
              this.globalProv.hideLoader();  
              this.globalProv.presentToast(res.message);
              this.idavatar = res.data; 
              this.globalProv.setStorage('user', {
                id: res.data,
              });
              //Si no encontro su eps y su arl si
              if(this.registerForm.controls.othereps.value != "" && this.registerForm.controls.otherarl.value == ""){
              //Si no encontro su arl y su eps si
              this.verify();
              }else if(this.registerForm.controls.otherarl.value != "" && this.registerForm.controls.othereps.value == ""){
              this.verify();
              //Si no encontro ninguno de los 2
              }else if(this.registerForm.controls.otherarl.value != "" && this.registerForm.controls.othereps.value != ""){
              this.verify();
              }else if(this.registerForm.controls.othercompany.value != ""){
                this.verify();
              }
              
              //Si encontro tanto su eps como su arl
              if(this.registerForm.controls.eps.value != "other" && this.registerForm.controls.arl.value != "other"){
                this.takepicture();
                //this.router.navigateByUrl('/completeregister');
              }
            }
          });
        }
    }



    /*async openModal(one=null, two=null) {
      const modal = await this.modalController.create({
        component: OtherformPage,
        componentProps: {
          "needed1": one,
          "needed2": two
        }
      });
      return await modal.present();
    }*/


  /**
   * Other Functions
   */  
  verify(){
    //call provider API
    this.globalProv.getStorage("user").then(res => {
      //Si encontro su arl y su eps no
      if(this.epsselected == "other" && this.arlselected != "other"){
        let request = {
            count: "eps",
            eps: this.registerForm.controls.othereps.value,
            arl: 1,
            person_id: res.id
        }
        this.submit(request);  
      //Si encontro su eps y su arl no
      }else if(this.epsselected != "other" && this.arlselected == "other"){
        let request = {
            count: "arl",
            eps: 1,
            arl: this.registerForm.controls.otherarl.value,
            person_id: res.id
        }
        this.submit(request);
      //Si no encontro ninguno    
      }else if(this.epsselected == "other" && this.arlselected == "other"){
        let request = {
          count: "all",
            eps: this.registerForm.controls.othereps.value,
            arl: this.registerForm.controls.otherarl.value,
            person_id: res.id
        }
        this.submit(request); 
      }
      if(this.registerForm.controls.othercompany.value != ""){
        console.log("OTRA COMPAÑIA");
        let request = {
            company: this.registerForm.controls.othercompany.value,
            person_id: res.id
        }
        this.othercompany(request); 
      }
      
  });
}



submit(request){
      //call provider API
      this.EpsProv.setCallPromise('post', request, 'other', null).then(res => {    
          if(!res.success) {
            this.globalProv.presentToast(res.message);
            this.takepicture();
            //this.router.navigateByUrl("/completeregister");
          }else if(res.success) { 
            this.globalProv.presentToast(res.message);
            this.takepicture();
            //this.router.navigateByUrl("/completeregister");
          }
        });
}


takepicture(){
  this.globalProv.presentToast("Sonrie!");
  console.log("ENVIADO "+this.idavatar);
  const options: CameraOptions = {
    quality: 70,
    destinationType: this.camera.DestinationType.DATA_URL,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE,
    cameraDirection: this.camera.Direction.FRONT,
}
  
  this.camera.getPicture(options).then((imageData) => {
   // imageData is either a base64 encoded string or a file URI
   // If it's base64 (DATA_URL):
   this.base64Image = 'data:image/jpeg;base64,' + imageData;
   let request = {
    id: this.idavatar,
    avatar: this.base64Image,
  };
  //call provider API
  this.globalProv.showLoader();
  this.usersProv.setCallPromise('post', request, 'updateavatar', null).then(res => {
  this.globalProv.hideLoader();      
    if(!res.success) {
      this.globalProv.presentToast(res.message);
    }else if(res.success) {
      console.log("Actualizado");
      this.router.navigateByUrl('completeregister');
    }
  });
  }, (err) => {
   // Handle error
  });
}



  /**
  * Method get EPS
  */
  getEps() {
    //call provider API
    this.globalProv.showLoader();
      this.ArlProv.setCallObservable('get', 'geteps', null).subscribe
      (data => {
        this.globalProv.hideLoader();
        if (!data.success) {
          this.globalProv.presentToast(data.message);
        } else if (data.success) {
          for (let i = 0; i < data.data.length; i++) {
              this.eps.push({
              id_eps: data.data[i].id,
              eps: data.data[i].eps,
            });
          }
        }
      }); 
  }

/**
* Method get Arl
*/
 getArl() {
  //call provider API
  this.globalProv.showLoader();

    this.EpsProv.setCallObservable('get', 'getarl', null).subscribe
    (data => {
      this.globalProv.hideLoader();
      if (!data.success) {
        this.globalProv.presentToast(data.message);
      } else if (data.success) {
        for (let i = 0; i < data.data.length; i++) {
            this.arl.push({
            id_arl: data.data[i].id,
            arl: data.data[i].arl,
          });
        }
      }
    }); 
}

  /**
  * Method get Companies
  */
 getCompanies() {
  //call provider API
  this.globalProv.showLoader();
    this.companyProv.setCallObservable('get', 'getcompanies', null).subscribe
    (data => {
      this.globalProv.hideLoader();
      if (!data.success) {
        this.globalProv.presentToast(data.message);
      } else if (data.success) {
        console.log(data);
        for (let i = 0; i < data.data.length; i++) {
            this.companies.push({
            id_company: data.data[i].id,
            company: data.data[i].company,
          });
        }
      }
    }); 
}

/**
* Method get Companies
*/
getRh() {
  //call provider API
  this.globalProv.showLoader();
    this.RhProvider.setCallObservable('get', 'getrh', null).subscribe
    (data => {
      this.globalProv.hideLoader();
      if (!data.success) {
        this.globalProv.presentToast(data.message);
      } else if (data.success) {
        for (let i = 0; i < data.data.length; i++) {
            this.rh.push({
            id: data.data[i].id,
            rh: data.data[i].rh,
          });
        }
      }
    }); 
}


getRhSelect(rhcedula:string) {
  console.log("Traida");
  //call provider API
  this.globalProv.showLoader();
    this.RhProvider.setCallObservable('get', 'rh/'+rhcedula, null).subscribe
    (data => {
      this.globalProv.hideLoader();
      if (!data.success) {
        this.globalProv.presentToast(data.message);
      } else if (data.success) {
        this.rhexist = data.data[0].id;
        this.rhexistname = data.data[0].rh;
        console.table(data.data);
      }
    }); 
}


othercompany(request){
  console.log("ENVIANDO OTHER COMPANY");
  //call provider API
  this.EpsProv.setCallPromise('post', request, 'othercompany', null).then(res => {    
      if(!res.success) {
        this.globalProv.presentToast(res.message);
      }else if(res.success) { 
        this.globalProv.presentToast(res.message);
      }
    });
}

}
