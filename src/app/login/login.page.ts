import { Component, OnInit } from '@angular/core';
import { UserProvider} from "../../providers/users/user";
import { GlobalProvider} from "../../providers/global";
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  public loginForm: FormGroup;
  public verify = null;
  constructor(public userProv:UserProvider,public globalProv:GlobalProvider,public formBuilder: FormBuilder, public router:Router) {
    this.loginForm = formBuilder.group({
      email: ['', Validators.compose([Validators.maxLength(120), Validators.email, Validators.required])],
      password: ['', Validators.compose([Validators.required])],
  });

  }

  ngOnInit() {
  }


  login(){
    this.globalProv.getStorage("company").then(res => {
      this.verify = res.company_id;
    });
    if(this.verify == null){
      console.log("no existe");
    }else{
      this.router.navigateByUrl("/rules");
    }
    if(!this.loginForm.valid){
      this.globalProv.presentToast("Verifica la información");
    }else{
      let request = {
        email: this.loginForm.controls.email.value,
        password: this.loginForm.controls.password.value,
      };
      
      this.userProv.setCallPromise('post', request, 'loginapp', null).then(res => { 
        this.globalProv.showLoader();   
        if(!res.success) {
          this.globalProv.presentToast(res.message);
          this.globalProv.hideLoader();
          //this.router.navigateByUrl("/completeregister");
        }else if(res.success) {
          this.globalProv.hideLoader(); 
          //this.globalProv.presentToast(res.message);
          this.globalProv.setStorage('company', {
            company_id: res.data[0].company_id,
          });
          this.router.navigateByUrl("/rules");
        }
      });
    }
  }
}
