import { Component, OnInit } from '@angular/core';
import { GlobalProvider} from "../../providers/global";
import { RulesProvider} from "../../providers/rules/rules";
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-rules',
  templateUrl: './rules.page.html',
  styleUrls: ['./rules.page.scss'],
})

export class RulesPage implements OnInit {
  public configuration = {
    color_principal: "",
    color_secondary: "",
    terms: ""
  }
  public company_id;
  public rules = [];
  public count;
  slideOptions = {
    initialSlide: 0,
    slidesPerView: 1,
    autoplay:true
  };
  constructor(public globalProv: GlobalProvider, public router: Router, public rulProv: RulesProvider, public alertController: AlertController ) { }

  ngOnInit() {
    this.globalProv.getStorage("company").then(res => {
      this.getConfiguration(res.company_id);
      this.getRules(res.company_id);
    })
    
  }

  /*ionViewWillLeave(){
    window.location.reload();
  }*/

  /**
  * Method get Configuration 
  */
 getConfiguration(id_company) {
  //call provider API
  this.globalProv.showLoader();
    this.rulProv.setCallObservable('get', 'getconfiguration/'+id_company, null).subscribe
    (data => {
      this.globalProv.hideLoader();
      if (!data.success) {
        this.globalProv.presentToast(data.message);
      } else if (data.success) {
        this.configuration.color_principal = data.data[0].color_principal;
        this.configuration.color_secondary = data.data[0].color_secondary;
        this.configuration.terms = data.data[0].terms;
        this.globalProv.setStorage('configuration', {
          id: data.data[0].id,
          color_principal: data.data[0].color_principal,
          color_secondary: data.data[0].color_secondary,
          terms: data.data[0].terms,
          logo: data.data[0].logo,
        });
      }
    }); 
  }

  /**
  * Method get EPS
  */
 getRules(company_id) {
  //call provider API
  this.globalProv.showLoader();
    this.rulProv.setCallObservable('get', 'getrules/'+company_id, null).subscribe
    (data => {
      this.globalProv.hideLoader();
      if (!data.success) {
        this.globalProv.presentToast(data.message);
      } else if (data.success) {
        this.count = data.data.length;
        console.log("Este es el numero de paginas que hay ----"+this.count);
        for (let i = 0; i < data.data.length; i++) {
            this.rules.push({
            id: data.data[i].id,
            title: data.data[i].title,
            image: data.data[i].image,
            descriptions: ({
              description: data.data[i].description
            })
          });
        }
      }
    }); 
}

async Confirmar() {
  const alert = await this.alertController.create({
    header: 'Términos y Condiciones',
    message: this.configuration.terms,
    buttons: [
      {
        text: 'Cancelar',
        role: 'cancel',
        cssClass: 'secondary',
        handler: (blah) => {
          console.log('Confirm Cancel: blah');
        }
      }, {
        text: 'Aceptar',
        handler: () => {
         this.router.navigateByUrl('/home');
        }
      }
    ]
  });

  await alert.present();
}
  

}
