import { Component, OnInit } from '@angular/core';
import { GlobalProvider} from "../../providers/global";
import { UserProvider} from "../../providers/users/user";
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { EpsProvider} from "../../providers/eps/eps";
import { ArlProvider} from "../../providers/arl/arl";
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { RhProvider} from "../../providers/rh/rh";
import { CompanyProvider} from "../../providers/companies/company";
import { FormBuilder, FormGroup, Validators, NgControlStatus} from '@angular/forms';


@Component({
  selector: 'app-data',
  templateUrl: './data.page.html',
  styleUrls: ['./data.page.scss'],
})
export class DataPage implements OnInit {
  public updateForm: FormGroup;
  public id;
  public new = false;
  public idexist;
  public idavatar;
  public email;
  public name;
  public lastname;
  public phone;
  public sendmail;
  public sendphone;
  public sendname;
  public sendlastname;
  public type;
  public datauser=[];
  public arl=[];
  public rh=[];
  public showotherarl;
  public showothereps;
  public showothercompany;
  public eps=[];
  public epsselected;
  public epsselec;
  public rhselected;
  public contact;
  public telcontact;
  public companies = [];
  public companyselected;
  public arlselected;
  public arlchange;
  public epschange;
  public companychangeexist;
  public arlchangeexist;
  public epschangeexist;
  public companychange;
  public base64image;
  constructor(private usersProv: UserProvider, 
    private globalProv: GlobalProvider,
    public router:Router,
    private EpsProv: EpsProvider,
    private ArlProv: ArlProvider,
    private RhProvider: RhProvider,
    private companyProv: CompanyProvider,
    public formBuilder: FormBuilder,
    public alertController: AlertController,
    private camera:Camera) {
      this.updateForm = formBuilder.group({
        name: ['', Validators.compose([Validators.maxLength(60), Validators.pattern('[a-zA-Z ]*')])],
        lastname: ['', Validators.compose([Validators.maxLength(60), Validators.pattern('[a-zA-Z ]*')])],
        email: ['', Validators.compose([Validators.maxLength(120), Validators.email])],
        phone: ['', Validators.compose([Validators.minLength(10), Validators.maxLength(10), Validators.pattern('[0-9\/]*')])],
        contact: ['', Validators.compose([])],
        /*rh: [''],
        eps: ['',],
        arl: ['',],
        company: ['',],*/
        telcontact: ['', Validators.compose([Validators.minLength(10), Validators.maxLength(10), Validators.pattern('[0-9\/]*')])],
        othereps: ['', Validators.compose([Validators.maxLength(60), Validators.pattern('[a-zA-Z ]*')])],
        otherarl: ['', Validators.compose([Validators.maxLength(60), Validators.pattern('[a-zA-Z ]*')])],   
        othercompany: ['', Validators.compose([Validators.maxLength(60), Validators.pattern('[a-zA-Z ]*')])],   
    });

    }

  ngOnInit() {
    this.getExist();
    this.getCompanies();
    this.getArl();
    this.getRh();
    this.getEps();
  }

  public onChangeTimeEps(epssele){
    console.log(epssele);
    this.epschange = epssele;
    if(epssele == "other"){
      this.showothereps = true;
    }else{
      this.showothereps = false;
    }

  }
  public onChangeTimeArl(arlsele){
    this.arlchange = arlsele;
    console.log(arlsele);
    if(arlsele == "other"){
      this.showotherarl = true;
    }else{
      this.showotherarl = false;
    }
  }

  public onChangeTimeCompany(companysele){
    this.companychange = companysele;
    console.log(companysele);
    if(companysele == "other"){
      this.showothercompany = true;
    }else{
      this.showothercompany = false;
    }
  }

  getExist(){
    this.globalProv.getStorage("document").then(res => {
      this.idexist = res.document;
      this.type = res.type;
      this.getData(this.idexist, this.type);
    })
    .catch(err => {
      this.router.navigateByUrl('/');
      //show alert
    });
  }

  /**
  * Method get Companies
  */
 getData(id:number, type:number) {
  //call provider API
  this.globalProv.showLoader();
    this.usersProv.setCallObservable('get', 'userby/'+id+"/"+type, null).subscribe
    (data => {
      if (!data.success) {
        this.globalProv.hideLoader();
        this.globalProv.presentToast(data.message);
        this.globalProv.clearStorage();
        this.router.navigateByUrl("/document");
      } else if (data.success) {
          this.globalProv.hideLoader();
          this.globalProv.setStorage('user', {
            id: data.data[0].id,
          });
          this.idavatar = data.data[0].id;
          this.id = data.data[0].id;
          this.datauser.push({
            id: data.data[0].id,
            name: data.data[0].name,
            lastname: data.data[0].lastname,
            avatar: data.data[0].avatar,
            email: data.data[0].email,
            phone: data.data[0].phone,
            type: data.data[0].type_document_id1,
            arl_id: data.data[0].arl_id,
            eps_id: data.data[0].eps_id,
            company_id: data.data[0].company_id,
            contact_person: data.data[0].contact_person,
            number_person: data.data[0].number_person,
            rh_id: data.data[0].rh_id,
          });
          this.email = data.data[0].email;
          this.name = data.data[0].name;
          this.lastname = data.data[0].lastname;
          this.phone = data.data[0].phone;

          this.rhselected = data.data[0].rh_id;
          this.epsselected = data.data[0].eps_id;
          this.arlselected = data.data[0].arl_id;
          this.companyselected = data.data[0].company_id;
        //this.router.navigateByUrl("/data");
      }
    }); 
}


async presentAlertConfirm() {
  const alert = await this.alertController.create({
    header: 'Confirma!',
    message: '<strong>Tus datos son correctos?</strong>',
    buttons: [
      {
        text: 'Cancelar',
        role: 'cancel',
        cssClass: 'secondary',
        handler: (blah) => {
          console.log('Confirm Cancel: blah');
        }
      }, {
        text: 'Confirmar',
        handler: () => {
          this.updateData();
          this.router.navigateByUrl("/completeregister");
        }
      }
    ]
  });
  await alert.present();
}


updateData(){
      console.log(this.arlchange+'asd');
      if (!this.updateForm.valid) {
        this.globalProv.presentToast("Revisa los datos ingresados");
      }else{
        if(this.epsselected == "other"){
          this.eps
        }
        if(this.arlchange == "" || this.arlchange == undefined || this.arlchange == "other"){
          this.arlchangeexist = this.arlselected;
        }
        if(this.epschange == "" || this.epschange == undefined || this.epschange == "other"){
          this.epschangeexist = this.epsselected;
        }
        if(this.companychange == "" || this.companychange == undefined || this.companychange == "other"){
          this.companychangeexist = this.companyselected;
        }else{
          this.companychangeexist = this.companychange;
        }
        if(this.updateForm.controls.contact.value == ""){
          this.contact = "No tiene";
        }else{
          this.contact = this.updateForm.controls.contact.value;
        }
        if(this.updateForm.controls.telcontact.value == ""){
          this.telcontact = "No tiene";
        }else{
          this.telcontact = this.updateForm.controls.telcontact.value;
        }
        if(this.updateForm.controls.email.value == ""){
          this.sendmail = this.email;
        }else{
          this.sendmail = this.updateForm.controls.email.value;
        }
        if(this.updateForm.controls.name.value == ""){
          this.sendname = this.name;
        }else{
          this.sendname = this.updateForm.controls.name.value;
        }
        if(this.updateForm.controls.lastname.value == ""){
          this.sendlastname = this.lastname;
        }else{
          this.sendlastname = this.updateForm.controls.lastname.value;
        }
        if(this.updateForm.controls.phone.value == ""){
          this.sendphone = this.phone;
        }else{
          this.sendphone = this.updateForm.controls.phone.value;
        }
        let request = {
                id: this.id,
                name: this.sendname,
                lastname: this.sendlastname,
                phone: this.sendphone,
                eps: this.epschangeexist,
                arl: this.arlchangeexist,
                email: this.sendmail,
                company: this.companychangeexist,
                contact: this.contact,
                numcontact: this.telcontact,
                rh: this.rhselected
            };

        //call provider API
        this.globalProv.showLoader();
        this.usersProv.setCallPromise('post', request, 'updatedata', null).then(res => {
          this.globalProv.hideLoader();      
            if(!res.success) {
              this.globalProv.presentToast(res.message);
            }else if(res.success) {
              console.log("Actualizado");
              this.verify();
              this.router.navigateByUrl('/completeregister');
            }
          });
      }
}
showVal(value){
  this.rhselected = value;
}

takepicture(){
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      cameraDirection: this.camera.Direction.FRONT,
  }
    
    this.camera.getPicture(options).then((imageData) => {
      this.new = true;
     // imageData is either a base64 encoded string or a file URI
     // If it's base64 (DATA_URL):
     this.base64image = 'data:image/png;base64,' + imageData;
     console.log("USUARIO "+this.idavatar);
     let request = {
      id: this.idavatar,
      avatar: this.base64image,
    };

  //call provider API
  this.globalProv.showLoader();
  this.usersProv.setCallPromise('post', request, 'updateavatar', null).then(res => {
  this.globalProv.hideLoader();      
    if(!res.success) {
      this.globalProv.presentToast(res.message);
    }else if(res.success) {
      console.log("Actualizado");
      this.globalProv.presentToast("Foto Actualizada Correctamente");
    }
  });
    }, (err) => {
      console.log("ERROR");
     // Handle error
    });
  }
  /**
   * Other Functions
   */  
  verify(){
    console.log("VERIFICANDO");
    //call provider API
    this.globalProv.getStorage("user").then(res => {
      console.log('Probando');
      //Si encontro su arl y su eps no
      if(this.epschange == "other" && this.arlchange != "other"){
        let request = {
            count: "eps",
            eps: this.updateForm.controls.othereps.value,
            arl: this.arlselected,
            person_id: res.id
        }
        this.submit(request);  
      //Si encontro su eps y su arl no
      }else if(this.epschange != "other" && this.arlchange == "other"){
        let request = {
            count: "arl",
            eps: this.epschange,
            arl: this.updateForm.controls.otherarl.value,
            person_id: res.id
        }
        this.submit(request);
      //Si no encontro ninguno    
      }else if(this.epschange == "other" && this.arlchange == "other"){
        console.log("Los 2");
        let request = {
          count: "all",
            eps: this.updateForm.controls.othereps.value,
            arl: this.updateForm.controls.otherarl.value,
            person_id: res.id
        }
        this.submit(request); 
      }
      //OTRA EMPRESA
      if(this.companychange == "other"){
        let request = {
            company: this.updateForm.controls.othercompany.value,
            person_id: res.id
        }
        this.othercompany(request); 
      }
      
  });
}

othercompany(request){
  console.log("ENVIANDO OTHER COMPANY");
  //call provider API
  this.EpsProv.setCallPromise('post', request, 'othercompany', null).then(res => {    
      if(!res.success) {
        this.globalProv.presentToast(res.message);
      }else if(res.success) { 
        this.globalProv.presentToast(res.message);
      }
    });
}


submit(request){
  console.log("ENVIANDO OTHER");
  //call provider API
  this.EpsProv.setCallPromise('post', request, 'other', null).then(res => {    
      if(!res.success) {
        this.globalProv.presentToast(res.message);
      }else if(res.success) { 
        this.globalProv.presentToast(res.message);
      }
    });
}
  /**
  * Method get EPS
  */
 getEps() {
  //call provider API
    this.ArlProv.setCallObservable('get', 'geteps', null).subscribe
    (data => {
      if (!data.success) {
        this.globalProv.presentToast(data.message);
      } else if (data.success) {
        for (let i = 0; i < data.data.length; i++) {
            this.eps.push({
            id_eps: data.data[i].id,
            eps: data.data[i].eps,
          });
        }
      }
    }); 
}

/**
* Method get Arl
*/
getArl() {
//call provider API
  this.EpsProv.setCallObservable('get', 'getarl', null).subscribe
  (data => {
    if (!data.success) {
      this.globalProv.presentToast(data.message);
    } else if (data.success) {
      for (let i = 0; i < data.data.length; i++) {
          this.arl.push({
          id_arl: data.data[i].id,
          arl: data.data[i].arl,
        });
      }
    }
  }); 
}

  /**
  * Method get Companies
  */
 getCompanies() {
  //call provider API
    this.companyProv.setCallObservable('get', 'getcompanies', null).subscribe
    (data => {
      if (!data.success) {
        this.globalProv.presentToast(data.message);
      } else if (data.success) {
        for (let i = 0; i < data.data.length; i++) {
            this.companies.push({
            id_company: data.data[i].id,
            company: data.data[i].company,
          });
        }
      }
    }); 
}

/**
* Method get Companies
*/
 getRh() {
  //call provider API
    this.RhProvider.setCallObservable('get', 'getrh', null).subscribe
    (data => {
      if (!data.success) {
        this.globalProv.presentToast(data.message);
      } else if (data.success) {
        for (let i = 0; i < data.data.length; i++) {
            this.rh.push({
            id: data.data[i].id,
            rh: data.data[i].rh,
          });
        }
      }
    }); 
}

}
