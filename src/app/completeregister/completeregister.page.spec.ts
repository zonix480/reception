import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompleteregisterPage } from './completeregister.page';

describe('CompleteregisterPage', () => {
  let component: CompleteregisterPage;
  let fixture: ComponentFixture<CompleteregisterPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompleteregisterPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompleteregisterPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
