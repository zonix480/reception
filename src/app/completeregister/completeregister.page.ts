import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule} from '@angular/forms';
import { Router } from '@angular/router';
import { GlobalProvider} from "../../providers/global";
import { UserProvider} from "../../providers/users/user";
import { CompanyProvider} from "../../providers/companies/company";

@Component({
  selector: 'app-completeregister',
  templateUrl: './completeregister.page.html',
  styleUrls: ['./completeregister.page.scss'],
})
export class CompleteregisterPage implements OnInit {
  public finalizeForm: FormGroup;
  public isEnabled:boolean = true;
  public companies = [];
  public users = [
  ];
  public items =[];
  public visitor:any;
  public waste;
  //Get value on ionChange on IonRadioGroup
  selectedRadioGroup:any;
  //Get value on ionSelect on IonRadio item
  selectedRadioItem:any;
 

  public companyselected = "";
  public company_id;
  isItemAvailable = false; // initialize the items with false
  public configuration = {
    id:"",
    color_principal: "",
    color_secondary: "",
    terms: "",
    logo:""
  }

  constructor(private usersProv: UserProvider,
    private globalProv: GlobalProvider,
    public companyProv: CompanyProvider,
    public formBuilder: FormBuilder,
    public router: Router) {
    this.initializeItems();
  }

  ngOnInit() {
    this.globalProv.getStorage("configuration").then(res => {
      this.configuration.id = res.id,
      this.configuration.color_principal = res.color_principal,
      this.configuration.color_secondary = res.color_secondary,
      this.configuration.logo = res.logo
    })
    this.globalProv.getStorage("company").then(res => {
      this.company_id = res.company_id
      this.getUsersByCompany(res.company_id);
    });
    this.globalProv.hideLoader();
  }

  final(){
    if (this.selectedRadioGroup.value === "") {
      this.globalProv.presentToast("Revisa los datos ingresados");
    }else{
      console.log(this.visitor);
      this.createvisit();
       this.router.navigateByUrl('/rules');
    }
  }



 /**
  * Method get UsersCompany
  */
 getUsersByCompany(idcompany:any) {
   this.users=[];
  //call provider API
  this.globalProv.showLoader();
    this.companyProv.setCallObservable('get', 'userscompany/'+idcompany, null).subscribe
    (data => {
      this.globalProv.hideLoader();
      if (!data.success) {
        this.globalProv.presentToast(data.message);
      } else if (data.success) {
        for (let i = 0; i < data.data.length; i++) {
            this.users.push({
            id_user: data.data[i].id,
            name: data.data[i].name + " " + data.data[i].lastname,
          });
        }
      }
    }); 
}


createvisit(){
  this.globalProv.showLoader();
  this.globalProv.getStorage("user").then(res => {
  this.visitor = (res.id);
        let request = {
          company_id: this.company_id,
          visitor_id: this.visitor,
          person_id: Number(this.selectedRadioGroup.value)
      };
      //call provider API
      this.usersProv.setCallPromise('post', request, 'createvisit', null).then(res => {
        this.globalProv.hideLoader();      
          if(!res.success) {
            this.globalProv.presentToast(res.message);
          }else if(res.success) { 
            this.globalProv.presentToast(res.message);
            this.globalProv.clearStorage();
            this.globalProv.setStorage('company', {
              company_id: this.company_id,
            });
            this.globalProv.getStorage("company").then(res => {
              this.getConfiguration(res.company_id);
            })
          }
        });
      });
  }

  initializeItems() {
    this.items = [
      'Islamabad',
      'Istanbul',
      'Jakarta',
      'Kiel',
      'Kyoto',
      'Le Havre',
      'Lebanon',
      'Lhasa',
    ];
  }

  getItems(ev) {
    // Reset items back to all of the items
    
    // set val to the value of the ev target
    var val = ev.target.value;
    if(val == ""){
      this.getUsersByCompany(this.company_id);
    }

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.users = this.users.filter((item) => {
        return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  onClick( check ){
    console.table(check);
  }

  test(){
    console.log("PERSONA "+ this.selectedRadioGroup.value+ "---- EMPRESA: ----"+ this.companyselected );
    //console.log(this.waste);
  }


  radioGroupChange(event) {
    console.log("radioGroupChange",event.detail);
    this.selectedRadioGroup = event.detail;
  }
 
  radioFocus() {
    console.log("radioFocus");
  }
  radioSelect(event) {
    console.log("radioSelect",event.detail);
    this.selectedRadioItem = event.detail;
  }
  radioBlur() {
    console.log("radioBlur");
  }
 /**
  * Method get Configuration 
  */
 getConfiguration(id_company) {
  //call provider API
  this.globalProv.showLoader();
    this.usersProv.setCallObservable('get', 'getconfiguration/'+id_company, null).subscribe
    (data => {
      this.globalProv.hideLoader();
      if (!data.success) {
        this.globalProv.presentToast(data.message);
      } else if (data.success) {
        this.configuration.color_principal = data.data[0].color_principal;
        this.configuration.color_secondary = data.data[0].color_secondary;
        this.configuration.terms = data.data[0].terms;
        this.globalProv.setStorage('configuration', {
          id: data.data[0].id,
          color_principal: data.data[0].color_principal,
          color_secondary: data.data[0].color_secondary,
          terms: data.data[0].terms,
          logo: data.data[0].logo,
        });
      }
    }); 
  }



}
