import { Component } from '@angular/core';
import { GlobalProvider} from "../providers/global";
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  public user:number = 0;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private globalProv:GlobalProvider,
    public router: Router
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.globalProv.clearStorage();
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      //this.router.navigateByUrl('signature')
    });
  }
}
