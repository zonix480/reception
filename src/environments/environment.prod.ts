/**
 * Environment production
 */
export var environment = {
  production: true,
  BASE_URL: 'http://172.16.16.155:8888/adminlte-generator/public/api/v1',
  //BASE_URL: 'http://172.16.108.131:8000/api/v1',
  //BASE_URL: 'http://172.16.16.107:8888/adminlte-generator/public/api/v1',
};
